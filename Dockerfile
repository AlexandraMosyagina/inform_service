FROM docker.io/tiangolo/uvicorn-gunicorn-fastapi:python3.11

WORKDIR /app

COPY . /app

RUN pip install poetry && \
    poetry config virtualenvs.create false && \
    poetry install --no-root --no-dev

CMD ["uvicorn", "main:app", "--host", "0.0.0.0", "--port", "8000"]





