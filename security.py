from datetime import datetime, timedelta
import jwt
from fastapi import HTTPException

SECRET_KEY = "secret"
TOKEN_EXPIRATION = timedelta(minutes=15)


def generate_token(username: str) -> str:
    payload = {
        "sub": username,
        "exp": datetime.utcnow() + TOKEN_EXPIRATION
    }
    token = jwt.encode(payload, SECRET_KEY, algorithm="HS256")
    return token


def validate_token(token: str) -> str:
    try:
        payload = jwt.decode(token, SECRET_KEY, algorithms=["HS256"])
        return payload["sub"]
    except jwt.ExpiredSignatureError:
        raise HTTPException(status_code=401, detail="Token has expired")
    except jwt.InvalidTokenError:
        raise HTTPException(status_code=401, detail="Invalid token")