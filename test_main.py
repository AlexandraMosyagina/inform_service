import pytest
from fastapi.testclient import TestClient
from main import app


@pytest.fixture
def client():
    return TestClient(app)


def test_successful_login(client):
    # test successful login
    response = client.post("/login", data={"username": "john", "password": "password123"})
    # check the response status code
    assert response.status_code == 200
    # check if John is redirected to the token page
    assert response.url.path == "/token/john"


def test_unsuccessful_login(client):
    # test unsuccessful login with wrong password
    response = client.post("/login", data={"username": "john", "password": "wrongpassword"})
    # check the response status code
    assert response.status_code == 400
    # check if the error message is displayed in the response HTML
    assert "Incorrect username or password." in response.text


def test_download_token_page_success(client):
    # test successful download of the token page
    response = client.post("/login", data={"username": "john", "password": "password123"})
    assert response.status_code == 200


def test_download_token_page_invalid_username(client):
    # test accessing the token page with an invalid username
    response = client.get("/token/unknown")
    # check the response status code
    assert response.status_code == 405


def test_download_token_page_unauthorized_access(client):
    # test unauthorized access to the token page
    response = client.get("/token/emma")
    # check the response status code
    assert response.status_code == 405


def test_generate_token_success(client):
    # test successful generation of a token
    response = client.get("/generate_token?username=john")
    # check the response status code
    assert response.status_code == 200
    # check if the response contains the token
    assert "token" in response.json()


def test_show_salary_success(client):
    # test successful display of salary information
    response = client.get("/salary/john")
    # check the response status code
    assert response.status_code == 200
    # check if the response contains the username, salary, and promotion date
    assert "john" in response.text
    assert "5000" in response.text
    assert "2023-07-01" in response.text


def test_show_salary_invalid_username(client):
    # test accessing salary information with an invalid username
    response = client.get("/salary/nonexistent_user")
    # check the response status code
    assert response.status_code == 200
    # check if the response contains the error message for an invalid username
    assert "Invalid username" in response.text
