import pytest
from datetime import datetime, timedelta
import jwt
from security import generate_token, validate_token
from fastapi import HTTPException

SECRET_KEY = "secret"


def test_generate_token():
    # test token generation
    username = "testuser"
    token = generate_token(username)
    assert isinstance(token, str)


def test_validate_token():
    # test token validation
    username = "testuser"
    token = generate_token(username)

    # test validation of a valid token
    validated_username = validate_token(token)
    assert validated_username == username

    # test validation of an expired token
    expired_token = generate_token(username)
    expired_token_payload = jwt.decode(expired_token, SECRET_KEY, algorithms=["HS256"])
    expired_token_payload["exp"] = datetime.utcnow() - timedelta(minutes=15)
    expired_token = jwt.encode(expired_token_payload, SECRET_KEY, algorithm="HS256")

    with pytest.raises(HTTPException) as e:
        validate_token(expired_token)

    assert e.value.status_code == 401
    assert e.value.detail == "Token has expired"

    # test validation of an invalid token
    invalid_token = "invalid_token"

    with pytest.raises(HTTPException) as e:
        validate_token(invalid_token)

    assert e.value.status_code == 401
    assert e.value.detail == "Invalid token"
