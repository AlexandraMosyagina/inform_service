# Service Setup and Usage Instructions

To run the service and interact with the project, please follow the instructions below:

## Prerequisites

Make sure you have the following installed on your system:

- Python (version 3.7 or higher)
- Poetry (a Python dependency management tool)
- Docker (for containerization)

## Installation

1. Clone the project repository:

    ```bash
    git clone <repository_url>
    ```

2. Navigate to the project directory:

    ```bash
    cd project-directory
    ```

3. Install project dependencies using Poetry:

    ```bash
    poetry install
    ```

## Running the Service

Start the service locally:

```bash
poetry run uvicorn main:app --reload
 ```
The service will be accessible at http://localhost:8000.

Open your web browser and visit http://localhost:8000 to access the start page.

## Service Endpoints

The service provides the following endpoints:

- GET /: Displays the start page.
- POST /login: Processes the login page form data.
- POST /token/{username}: Handles the request for the token page.
- GET /generate_token: Generates a token for a given username.
- GET /verify_token: Verifies the validity of a token.
- GET /salary/{username}: Displays salary information for a specific username.
- POST /salary/{username}: Updates the salary information for a specific username.
- Interacting with the Service
- Start by visiting http://localhost:8000 in your web browser.

## Instruction

1. Use the provided login form to enter a username and password.
2. Upon successful login, you will be redirected to the token page.
3. Generate a token by clicking the "Сгенерировать токен" button.
4. Use the generated token to verify by clicking the "Проверить токен" button.
5. Access salary information for a specific username.

## Docker Containerization
To run the service using Docker, follow these steps:

Build the Docker image:

```bash
docker build -t inform-service .
 ```

Run the Docker container:

```bash
docker run -p 8000:8000 inform-service
 ```

The service will be accessible at http://localhost:8000.

That's it! You now have the service up and running, ready for interaction.

## Fix problem with jwt
```bash
pip3 install pyjwt==1.5.3  
 ```
```bash
pip install --upgrade pyjwt
 ```
