from fastapi import FastAPI, Request, HTTPException, status
from fastapi.responses import HTMLResponse, RedirectResponse
from fastapi.templating import Jinja2Templates
from security import generate_token, validate_token

app = FastAPI()
templates = Jinja2Templates(directory="templates")

# dummy data for salary and promotion date
salary_data = {
    "john": {"salary": 5000, "promotion_date": "2023-07-01"},
    "emma": {"salary": 6000, "promotion_date": "2023-08-15"}
}

# user credentials for login
credentials = {
    "john": {"password": "password123"},
    "emma": {"password": "abc123"}
}


# send a user the start page
@app.get("/", response_class=HTMLResponse)
async def root(request: Request):
    return templates.TemplateResponse("index.html", {"request": request})


# receive form data and process the login page
@app.post("/login")
async def login(request: Request):
    form = await request.form()
    username = form.get("username")
    password = form.get("password")

    # check if the username and password match
    if username in credentials and credentials[username]["password"] == password:
        redirect_url = app.url_path_for("token", username=username)
        return RedirectResponse(redirect_url)
    else:
        error_message = "Incorrect username or password."
        return templates.TemplateResponse("index.html", {"request": request, "error_message": error_message},
                                          status_code=status.HTTP_400_BAD_REQUEST)


@app.post("/token/{username}", response_class=HTMLResponse, name="token")
async def download_token_page(request: Request, username: str):
    # handle request for token page and render token.html template
    return templates.TemplateResponse("token.html", {"request": request, "username": username,
                                                     "success_message": None, "error_message": None})


@app.get("/generate_token")
async def generate_token_endpoint(request: Request, username: str):
    # handle GET request to generate token
    token = generate_token(username)
    return {"token": token}


@app.get("/verify_token")
async def verify_token(request: Request, username: str, user_token: str):
    # handle GET request to verify token
    try:
        validate_token(user_token)
        return {"valid": True}
    except HTTPException as e:
        return {"valid": False, "error_message": str(e.detail)}


# display salary page with user-specific information
@app.get("/salary/{username}")
async def show_salary(request: Request, username: str):
    # check the username
    if username in salary_data:
        # get user data
        user_salary = salary_data[username]["salary"]
        promotion_date = salary_data[username]["promotion_date"]
        return templates.TemplateResponse(
            "salary.html",
            {"request": request, "username": username, "salary": user_salary, "promotion_date": promotion_date}
        )
    else:
        # handle invalid username
        return templates.TemplateResponse(
            "index.html",
            {"request": request, "error_message": "Invalid username."}
        )


# update salary information
@app.post("/salary/{username}")
async def submit_salary(request: Request, username: str, user_token: str, salary: int):
    # check the username
    if username in salary_data:
        # check the token
        try:
            validate_token(user_token)
            # update user salary
            salary_data[username]["salary"] = salary
            return templates.TemplateResponse(
                "salary.html",
                {"request": request, "username": username, "salary": salary,
                 "promotion_date": salary_data[username]["promotion_date"], "error_message": None}
            )
        except HTTPException as e:
            # handle unauthorized access
            return RedirectResponse("/", status_code=status.HTTP_401_UNAUTHORIZED)
    else:
        # handle invalid username
        return templates.TemplateResponse(
            "index.html",
            {"request": request, "error_message": "Invalid username."}
        )
